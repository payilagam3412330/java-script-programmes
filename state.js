//create div in java script
const button=document.querySelector('button')
const resultdiv=document.createElement('div')
resultdiv.id='result';
document.getElementById('wrapper').appendChild(resultdiv)

button.addEventListener('click' ,display)

//create paragraph in new creared div
function display(){
    const input=document.querySelector('#input')
    const city=input.options[input.selectedIndex].value
    console.log(city)
     
    let population=0,GrowthRate=0,language=""
    switch(city){
        case'Chennai':
            population = 11933000
            GrowthRate = 2.37
            language = 'Tamil'
            break;
        case'Bengaluru':
            population = 13608000
            GrowthRate = 3.15
            language = 'Kannada'
            break;
        case'Mumbai':
            population = 21296517
            GrowthRate = 1.6
            language = 'Marathi'
            break;
    }

    let text=`The current population of ${city} is ${population} as of today, based on worldometer elaboration of the latest united Nations data. Growing rate is ${GrowthRate} People who spoken language is ${language}.`
    document.getElementById('result').innerHTML=text
}